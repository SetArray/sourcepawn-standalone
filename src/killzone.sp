#pragma semicolon 1

#define PLUGIN_AUTHOR "Tyler Smith"
#define PLUGIN_VERSION "1.03"

#include <sourcemod>
#include <sdkhooks>
#include <sdktools>

new killerClient = -1;
new isButtonPressable = 1;

public Plugin myinfo = 
{
	name = "Killzone'",
	author = PLUGIN_AUTHOR,
	description = "Plugin for Killzone events",
	version = PLUGIN_VERSION,
	url = ""
};

public OnPluginStart()
{
	SetConVarInt(FindConVar("mp_teammates_are_enemies"), 1);
	
	HookEventEx("round_start", Event_RoundStart, EventHookMode_Post);
	HookEvent("player_death", Event_PlayerDeath);
	HookEntityOutput("func_button", "OnPressed", btnPress);
}

public OnClientPutInServer(client)
{
    SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
}

public Action:Event_RoundStart(Handle:event, const String:name[], bool:dontBroadcast)
{
	killerClient = -1;
	isButtonPressable = 1;
}

public Action:Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (getAliveCount() == 1)
	{
		for(new i = 1; i <= MaxClients; i++)
		{
			if(IsPlayerAlive(i))
			{
				PrintToChatAll("[SM] Player %N has won the round!", i);
				ForcePlayerSuicide(i);
			}
		}
	}
	
	return Plugin_Continue;
}

public int getAliveCount()
{
	new count = 0;
	
	for(new i = 1; i <= MaxClients; i++)
	{
		if(IsClientInGame(i) && (!IsFakeClient(i)) && IsPlayerAlive(i))
		{
			count++;
		}
	}
	
	return count;
}

public btnPress(const String:output[], caller, attacker, Float:Any)
{
	if(isButtonPressable == 1)
	{
		killerClient = attacker;
		isButtonPressable = 0;
	}
}

public Action:OnTakeDamage(client, &attacker, &inflictor, &Float:damage, &damagetype)
{
	if (attacker != killerClient || damagetype == DMG_FALL)
    {
    	damage = 0.0;
    	
        return Plugin_Changed;
	}
	
	ForcePlayerSuicide(client);
	return Plugin_Handled;
}