#pragma semicolon 1

#define PLUGIN_AUTHOR "Tyler Smith"
#define PLUGIN_VERSION "1.00"

#include <sourcemod>
#include <sdktools>

new sounds[2048];
new soundCount;
new bool:musicDisabled[MAXPLAYERS + 1];

public Plugin myinfo = 
{
	name = "Stop Music",
	author = PLUGIN_AUTHOR,
	description = "Stops ambient music",
	version = PLUGIN_VERSION,
	url = ""
};

public void OnPluginStart()
{
	CreateConVar("sm_stopmusic_version", PLUGIN_VERSION, "Stop Map Music", FCVAR_NOTIFY|FCVAR_DONTRECORD);
	HookEvent("round_start", Event_RoundStart, EventHookMode_PostNoCopy);
	RegConsoleCmd("sm_music", Command_StopMusic, "Toggles map music");
	
	soundCount = 0;
}

public OnClientDisconnect_Post(client)
{
	
}

public Event_RoundStart(Handle:event, const String:name[], bool:dontBroadcast)
{
	
}

public OnEntityCreated(entity, const String:classname[])
{
	if(StrEqual(classname, "ambient_generic", false))
	{
		StopSoundAny()
	}
}

public Action:Command_StopMusic(client, args)
{

}

stock Client_StopSound(client, entity, channel, const String:name[])
{

}