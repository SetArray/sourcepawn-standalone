#include <sourcemod>
#include <sdktools>
#include <multicolors>

#pragma semicolon 1

#define PLUGIN_AUTHOR "Tyler Smith"
#define PLUGIN_VERSION "1.00"

public Plugin myinfo = 
{
	name = "Private Mute",
	author = PLUGIN_AUTHOR,
	description = "Client-sided muting players",
	version = PLUGIN_VERSION,
	url = ""
};

public void OnPluginStart()
{
	CreateConVar("sm_private_mute", PLUGIN_VERSION, "Private Mute Version", FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	RegConsoleCmd("sm_pmute", CommandPmute, "Mute a player for your client");
	RegConsoleCmd("sm_unpmute", CommandUnPmute, "Unmute a player for your client");
}

public Action:CommandPmute(client, args)
{
	if(client == 0)
	{
		PrintToChat(client, "[SM] You cannot use /pmute from this client");
		return Plugin_Handled;
	}
	if(IsClientAdmin(client))
	{
		PrintToChat(client, "[SM] Admins are not allowed to use this command!");
		return Plugin_Handled;
	}
	if (args == 0) 
	{
		PrintToChat(client, "[SM] Usage: /pmute <player>");
		return Plugin_Handled;
	}
	
	new target;
	decl String:targetStr[128];
	GetCmdArgString(targetStr, sizeof(targetStr));
	target = FindTarget(client, targetStr, true, false);
	
	if(target == -1)
	{
		PrintToChat(client, "[SM] Unable to find player!");
		return Plugin_Handled;
	}
	if(IsClientAdmin(target))
	{
		PrintToChat(client, "[SM] You cannot use this command on an admin!");
		return Plugin_Handled;
	}
	
	Pmute(client, target);
	return Plugin_Handled;
}

public Action:CommandUnPmute(client, args)
{
	if(client == 0)
	{
		PrintToChat(client, "[SM] You cannot use /unpmute from this client");
		return Plugin_Handled;
	}
	if(IsClientAdmin(client))
	{
		PrintToChat(client, "[SM] Admins are not allowed to use this command!");
		return Plugin_Handled;
	}
	if (args == 0) 
	{
		PrintToChat(client, "[SM] Usage: /unpmute <player>");
		return Plugin_Handled;
	}
	
	new target;
	decl String:targetStr[128];
	GetCmdArgString(targetStr, sizeof(targetStr));
	target = FindTarget(client, targetStr, true, false);
	
	if(target == -1)
	{
		PrintToChat(client, "[SM] Unable to find player!");
		return Plugin_Handled;
	}
	if(IsClientAdmin(target))
	{
		PrintToChat(client, "[SM] You cannot use this command on an admin!");
		return Plugin_Handled;
	}
	
	UnPmute(client, target);
	return Plugin_Handled;
}

public Pmute(client, target)
{
    SetListenOverride(client, target, Listen_No);
    decl String:targetName[256];
    GetClientName(target, targetName, sizeof(targetName));
    
    PrintToChat(client, "[SM] You muted %s for your client", targetName);
}

public UnPmute(client, target)
{
	SetListenOverride(client, target, Listen_Yes);

	decl String:targetName[256];
	GetClientName(target, targetName, sizeof(targetName));

	PrintToChat(client, "[SM] You unmuted %s for your client", targetName);
}

public bool IsClientAdmin(client)
{
	return CheckCommandAccess(client, "sm_kick", ADMFLAG_KICK);
}