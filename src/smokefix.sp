#pragma semicolon 1

#define PLUGIN_AUTHOR "Tyler Smith"
#define PLUGIN_VERSION "1.00"

#include <sourcemod>
#include <sdkhooks>
#include <sdktools>

public Plugin myinfo = 
{
	name = "SmokeFix",
	author = PLUGIN_AUTHOR,
	description = "Fixes infinite collision between grenades & breakable entities",
	version = PLUGIN_VERSION,
	url = ""
};

public void OnPluginStart() {}

public OnEntityCreated(entity, const String:classname[])
{
	if(StrEqual(classname, "smokegrenade_projectile") || StrEqual(classname, "decoy_projectile"))
		CreateTimer(0.0, iTimer, EntIndexToEntRef(entity));
}


public Action:iTimer(Handle:timer, any:ref)
{
	new entity = EntRefToEntIndex(ref);

	if(entity > MaxClients)
	{
		SDKHookEx(entity, SDKHook_StartTouch, touch);
		SDKHookEx(entity, SDKHook_Touch, touch);
		SDKHookEx(entity, SDKHook_EndTouch, touch);
	}
}

public Action:touch(entity, other)
{
	if(other > MaxClients)
	{
		new String:classname[30];
		GetEntityClassname(other, classname, sizeof(classname));
		int i = GetEntProp(other, Prop_Data, "m_iHealth");
		
		if(StrEqual(classname, "func_breakable") && i == 0)
			AcceptEntityInput(entity, "Kill");
	}
}  