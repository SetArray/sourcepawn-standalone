#pragma semicolon 1

#define PLUGIN_AUTHOR "Tyler Smith"
#define PLUGIN_VERSION "1.00"

#include <sourcemod>
#include <sdktools>


public Plugin myinfo = 
{
	name = "NoTag",
	author = PLUGIN_AUTHOR,
	description = "Disables damage and 'tagging' (decreased movement speed from damage)",
	version = PLUGIN_VERSION,
	url = ""
};

public void OnPluginStart()
{
	HookEvent("player_spawn", OnPlayerSpawn);
}

public OnPlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	SetEntProp(client, Prop_Data, "m_takedamage", 0, 1);
}