//Includes
#include <sourcemod>
#include <sdkhooks>
#include <sdktools>

#pragma semicolon 1

//Version 1 Patch 1
#define VERSION "v2.0"

//Console Variables
new Handle:g_CvarVersion = INVALID_HANDLE;
new Handle:g_CvarIsEnabled = INVALID_HANDLE;
new Handle:g_CvarGunChainCap = INVALID_HANDLE;

new bool:g_IsEnabled = false;
new bool:g_blockDespawn = false; //Flag for despawn exceptions (See Exceptions)


new g_GunChainCap; //Number of guns player may drop before their oldest gun despawns (See Exceptions)
new g_iData[MAXPLAYERS+1][16];

new g_totalWeapons;
new g_weapons[2048];

new String:g_weaponExclusions[] =  { "healthshot", "grenade", "c4", "flashbang", "molotov", "decoy", "taser", "knife" }; // Things in the Weapon_ class that can be ignored

public Plugin:myinfo = 
{
	name = "Mercy Anti-Gun Spam (MAGS)",
	author = "Mercy",
	description = "Prevent spam of dropped weapons",
	version = VERSION,
	url = "null"
};


public OnPluginStart()
{
	g_CvarVersion = CreateConVar("sm_mags", VERSION, "Mercy Anti-Gun Spam version", FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	g_CvarIsEnabled = CreateConVar("sm_mags_enabled", "1", "Check if Mercy Anti-Gun Spam enabled", FCVAR_NONE, true, 0.0, true, 1.0);
	g_CvarGunChainCap = CreateConVar("sm_mags_chaincap", "3", "How many guns a player can drop before invoking despawn", FCVAR_NONE, false, 0.0, true, 16.0);
	g_IsEnabled = GetConVarBool(g_CvarIsEnabled);
	g_GunChainCap = GetConVarInt(g_CvarGunChainCap);
	g_totalWeapons = 0;
	
	SetConVarString(g_CvarVersion, VERSION);
	HookConVarChange(g_CvarVersion, OnCvarChanged);
	HookConVarChange(g_CvarIsEnabled, OnCvarChanged);
	HookConVarChange(g_CvarGunChainCap, OnCvarChanged);
	
	HookEventEx("round_start", Event_RoundStart, EventHookMode_Post);
}

/*
 * Clears weapon counter for new round
 */
public Action:Event_RoundStart(Handle:event, const String:name[], bool:dontBroadcast)
{
	g_totalWeapons = 0; //Marks array data of existing weapons as cleared for rewrite
}

/*
 * Creates client hooks
 * Registers client ID
 */
public OnClientPutInServer(client) 
{ 	
	g_iData[client][0] = 0;
	SDKHook(client, SDKHook_WeaponDrop, Event_WeaponDrop);
	SDKHook(client, SDKHook_WeaponEquip, Event_WeaponEquip);
} 

/*
 * Hooks button-spawned weapons using the game_player_equip mechanic
 */
public OnEntityCreated(entity, const String:classname[]) 
{
	if (g_IsEnabled && StrEqual(classname, "game_player_equip", false))
		SDKHook(entity, SDKHook_Spawn, OnEntitySpawn);
}

// Triggers client's WeaponEquip for button-spawned weapons 
public Action:OnEntitySpawn(entity, classname)
{
	new client = GetEntPropEnt(entity, Prop_Send, "m_hOwner");
	
	if(IsValidClient(client))
		Event_WeaponEquip(client, entity);
	
	return Plugin_Continue;
} 

/*
 * Pretty much just here here for data management and bug prevention (See Exceptions)
 */
public Action:Event_WeaponEquip(client, weapon)
{
	char classname[128];
	if(IsValidEdict(weapon))
		GetEdictClassname(weapon, classname, sizeof(classname));
	
	if(g_IsEnabled)
	{
		removeWeaponFromList(weapon);
		
		new bool:isMatch = false;
		for (int i = 1; i <= g_GunChainCap; i++)
			if(weapon == g_iData[client][i])
				isMatch = true;
	
		if(isMatch == true)
			g_blockDespawn = true;
		else
			g_blockDespawn = false;
	}
}

/*
 * Registers weapon data for future processing
 * Handles despawning
 */
public Action:Event_WeaponDrop(client, weapon)
{
	char classname[128];
	if(IsValidEdict(weapon))
		GetEdictClassname(weapon, classname, sizeof(classname));
	
	//Handles despawning gun if necessary (See Exceptions)
	//If statement contains certain exclusions for efficiency in memory storage
	if(g_IsEnabled && !g_blockDespawn && StrContains(classname, "weapon_") != -1 && !isExcludedWeapon(classname))
	{
		g_weapons[g_totalWeapons] = weapon;
		g_totalWeapons++;
		
		new wIndex = g_iData[client][0] + 1;
		new oldestInChain = g_iData[client][wIndex];
		
		if(oldestInChain > MaxClients && IsValidEntity(oldestInChain) && IsValidEdict(oldestInChain) && isWeaponOnGround(oldestInChain))
		{
			AcceptEntityInput(oldestInChain, "Kill");
		}
		
		g_iData[client][wIndex] = weapon;
		
		if(wIndex >= g_GunChainCap) 
			wIndex = 0;
		g_iData[client][0] = wIndex;
	}
}

/*
 * Pretty straight forward :|
 */
public OnCvarChanged(Handle:convar, const String:oldValue[], const String:newValue[])
{
	if(convar == g_CvarVersion)
		SetConVarString(convar, VERSION);
		
	else if(convar == g_CvarIsEnabled)
		g_IsEnabled = GetConVarBool(g_CvarIsEnabled);
		
	else if(convar == g_CvarGunChainCap)
		g_GunChainCap = GetConVarInt(g_CvarGunChainCap);
}

/*
 * Sets data holder in array for guns to an invalid ID, marking it as no-action in processing
 */
public void removeWeaponFromList(int weapon)
{
	for (int i = 0; i < g_totalWeapons; i++)
	{
		if(g_weapons[i] == weapon)
			g_weapons[i] = -1;
	}
}

/*
 * Returns true if entity ID matches that of a registed ground weapon ID
 * Convenience function to make code easier to read
 */
public bool isWeaponOnGround(int weapon)
{
	for (int i = 0; i < g_totalWeapons; i++)
	{
		if(g_weapons[i] == weapon)
			return true;
	}
	
	return false;
}

/*
 * Checks if the entity is in the list of weapons that need to be ignored
 */
public bool isExcludedWeapon(char[] classname)
{
	for(int i = 0; i < sizeof(g_weaponExclusions); i++)
		if(StrContains(classname, g_weaponExclusions[i], false) != -1)
			return true;
			
	return false;
}

/*
 * Checks if the client is valid and in game
 */ 
stock bool:IsValidClient(client)
{ 
    if (client <= 0 || client > MaxClients || !IsClientConnected(client) || (IsFakeClient(client)))
    {
        return false; 
    }
    
    return IsClientInGame(client); 
} 

/*
 * ~~EXCEPTIONS~~ (exceptions to normal gun-despawning behavior)
 *
 * If a player picks up another player's dropped weapon, the previous owner of the gun's
 * weapon chain will be modified to prevent unintention lowering/raising of their gun chain cap or 
 * despawning a weapon while it is in another player's hand.
 *
 * If a player picks up their own dropped weapon, that weapon's placement in their chain will
 * be modified, and its previous placement will be cleared for rewrite to prevent unintentional
 * raising/lowering of their gun chain cap.
 */