#pragma semicolon 1

#include <sourcemod>
#include <sdkhooks>
#include <sdktools>
#include <cstrike>

#include <ttt_shop>
#include <ttt>
#include <config_loader>
#include <multicolors>

#define SHORT_NAME "ths"

#define PLUGIN_NAME TTT_PLUGIN_NAME ... " - Items: Toxic Healthshot"

new Handle:d_timer = INVALID_HANDLE;
new Handle:hs_timer = INVALID_HANDLE;

int g_iPrice = 0;

int g_iPrio = 0;

int g_iCount = 0;
int g_iPCount[MAXPLAYERS + 1] =  { 0, ... };
int g_toxicShots[MAXPLAYERS + 1] = {-1, ... };
int g_totalShots = 0;

char g_sConfigFile[PLATFORM_MAX_PATH] = "";
char g_sPluginTag[PLATFORM_MAX_PATH] = "";
char g_sLongName[64];

public Plugin myinfo =
{
	name = PLUGIN_NAME,
	author = TTT_PLUGIN_AUTHOR,
	description = TTT_PLUGIN_DESCRIPTION,
	version = TTT_PLUGIN_VERSION,
	url = TTT_PLUGIN_URL
};

public void OnPluginStart()
{
	TTT_IsGameCSGO();
	
	LoadTranslations("ttt.phrases");
	
	BuildPath(Path_SM, g_sConfigFile, sizeof(g_sConfigFile), "configs/ttt/config.cfg");
	Config_Setup("TTT", g_sConfigFile);
	
	Config_LoadString("ttt_plugin_tag", "{orchid}[{green}T{darkred}T{blue}T{orchid}]{lightgreen} %T", "The prefix used in all plugin messages (DO NOT DELETE '%T')", g_sPluginTag, sizeof(g_sPluginTag));
	
	Config_Done();
	
	BuildPath(Path_SM, g_sConfigFile, sizeof(g_sConfigFile), "configs/ttt/toxichealthshot.cfg");

	Config_Setup("TTT-ToxicHealthshot", g_sConfigFile);
	Config_LoadString("ths_name", "Toxic Healthshot", "The name of the Toxic Healthshot in the Shop", g_sLongName, sizeof(g_sLongName));
	
	g_iPrice = Config_LoadInt("ths_traitor_price", 9000, "The amount of credits for toxic healthshot costs. 0 to disable.");
	g_iCount = Config_LoadInt("ths_traitor_count", 1, "The amount of usages for toxic healthshots per round. 0 to disable.");
	g_iPrio = Config_LoadInt("ths_traitor_sort_prio", 0, "The sorting priority of the toxic healthshots in the shop menu.");
	
	Config_Done();
	
	HookEvent("player_spawn", Event_PlayerSpawn);
	HookEvent("round_start", OnRoundStart, EventHookMode_PostNoCopy);
	HookEvent("weapon_fire", Event_WeaponFire);
}

public void OnClientDisconnect(int client)
{
	ResetHealthshot(client);
}

public Action:Event_WeaponFire(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	new ActiveWeapon = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
	
	new String:Weapon[32];
	GetEventString(event, "weapon", Weapon, 32);

	if(StrEqual(Weapon, "weapon_healthshot") || StrEqual(Weapon, "healthshot")) 
	{ 
		for (int i = 0; i < g_totalShots; i++)
		{
			if(ActiveWeapon == g_toxicShots[i] && GetClientHealth(client) < 100)
			{
				CPrintToChat(client, "{darkred}This needle stings more than usual...");
				g_toxicShots[i] = -1;
				
				d_timer = CreateTimer(1.5, DelayHealthShot, client);
				return Plugin_Handled;
			}
		}
	}
	
	return Plugin_Continue;
}

public Action:DelayHealthShot(Handle:timer, any:client)
{
	hs_timer = CreateTimer(0.5, Health_Shot, client, TIMER_REPEAT);
	
	CloseHandle(d_timer);
}

public Action:Health_Shot(Handle:timer, any:client)
{
	static int execCount = 0;
	
	if (execCount >= 15 || GetClientHealth(client) <= 5) 
	{
		execCount = 0;
		CloseHandle(hs_timer);
		return Plugin_Stop;
	}
	
	new health = GetClientHealth(client) - 5;
	if (health > 95)health = 95;
	
	SetEntityHealth(client, health);
	
	execCount++;
	return Plugin_Continue;
}

public Action:Event_PlayerSpawn(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	
	if (TTT_IsClientValid(client))
		ResetHealthshot(client);
}

public void OnAllPluginsLoaded()
{
	TTT_RegisterCustomItem(SHORT_NAME, g_sLongName, g_iPrice, TTT_TEAM_TRAITOR, g_iPrio);
}

public Action TTT_OnItemPurchased(int client, const char[] itemshort, bool count)
{
	if (TTT_IsClientValid(client) && IsPlayerAlive(client))
	{
		if (StrEqual(itemshort, SHORT_NAME, false))
		{
			if(g_iPCount[client] >= g_iCount)
			{
				CPrintToChat(client, g_sPluginTag, "Bought All", client, g_sLongName, g_iCount);
				return Plugin_Stop;
			}
			
			g_toxicShots[g_totalShots] = GivePlayerItem(client, "weapon_healthshot");
			g_totalShots++;
			
			if (count)
				g_iPCount[client]++;
		}
	}
	return Plugin_Continue;
}

void ResetHealthshot(int client)
{
	g_iPCount[client] = 0;
}

public OnRoundStart(Handle:event, const String:name[], bool:dontBroadcast) 
{ 
    for (int i = 0; i < g_totalShots; i++)
    	g_toxicShots[i] = -1;
    	
    g_totalShots = 0;
}  
