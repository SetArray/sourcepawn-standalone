# SourcePawn Collection

This is a general collection of SourcePawn code and plugins that I have written for various clients. SourcePawn is a scripting language for Source servers that can only be described as a mix between Java and C# with manual memory management.